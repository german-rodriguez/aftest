//
//  User.swift
//  AFTest
//
//  Created by German Rodriguez on 7/21/21.
//

import Foundation
import ObjectMapper

struct User: ImmutableMappable {
    
    let id: Int
    let name: String
    let username: String
    
    init(map: Map) throws {
        self.id = try map.value(Keys.id.rawValue)
        self.name = try map.value(Keys.name.rawValue)
        self.username = try map.value(Keys.username.rawValue)
    }
    
    enum Keys: String {
        case id
        case name
        case username
    }
}
