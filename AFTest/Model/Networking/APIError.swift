//
//  APIError.swift
//  AFTest
//
//  Created by German Rodriguez on 7/21/21.
//

import Foundation

enum APIError: Error {
    
    case logIn
    case createSession
    case getUser
    
    var localizedDescription: String {
        switch self {
        case .logIn: return "Log in error"
        case .createSession: return "Create session error"
        case .getUser: return "Get user error"
        }
    }
}
