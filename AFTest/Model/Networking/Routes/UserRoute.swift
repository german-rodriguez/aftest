//
//  UserRoute.swift
//  AFTest
//
//  Created by German Rodriguez on 7/21/21.
//

import Foundation
import Alamofire

enum UserRoute: APIRoute {
   
    case getAccount
    case setFavorite(accountId: Int, mediaId: Int, value: Bool)
    
    var method: HTTPMethod {
        switch self {
        case .getAccount: return .get
        case .setFavorite: return .post
        }
    }
    
    var sessionPolicy: APIRouteSessionPolicy { .privateDomain }
    
    func asURLRequest() throws -> URLRequest {
        let path: String
        let params: JSONDictionary
        switch self {
        case .getAccount:
            path = "account"
            params = [:]
        case .setFavorite(let accountId, let mediaId, let value):
            path = "/account/\(accountId)/favorite"
            params = [
                "media_type": "movie",
                "media_id": mediaId,
                "favorite": value,
            ]
        }
        return try self.encoded(path: path, params: params)
    }
}
