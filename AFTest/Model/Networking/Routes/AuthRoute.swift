//
//  AuthRoute.swift
//  AFTest
//
//  Created by German Rodriguez on 7/21/21.
//

import Foundation
import Alamofire

enum AuthRoute: APIRoute {

    case getAuthToken
    case login(requestToken: String, username: String, password: String)
    case createSession(String)

    var method: HTTPMethod {
        switch self {
        case .getAuthToken: return .get
        case .login, .createSession: return .post
        }
    }
    
    var sessionPolicy: APIRouteSessionPolicy { .publicDomain }
    
    func asURLRequest() throws -> URLRequest {
        let path: String
        let params: JSONDictionary
        
        switch self {
        case .getAuthToken:
            path = "authentication/token/new"
            params = [:]
        case .login(let requestToken, let username, let password):
            path = "/authentication/token/validate_with_login"
            params = [
                "username": username,
                "password": password,
                "request_token": requestToken,
            ]
        case .createSession(let requestToken):
            path = "authentication/session/new"
            params = ["request_token": requestToken]
        }
        
        return try self.encoded(path: path, params: params)
    }
}
