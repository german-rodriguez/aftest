//
//  SessionManager.swift
//  AFTest
//
//  Created by German Rodriguez on 7/21/21.
//

import Foundation

class SessionManager {
    
    static let shared = SessionManager()

    let apiKey = "1c3d7a8822e91df6a2d4d95a52b86470"
    private(set) var sessionId: String?
    private(set) var currentUser: User?
    
    private init() { }

    func logIn(username: String, password: String, onCompletion: @escaping APIResponse) {
        APIClient.shared.request(request: AuthRoute.getAuthToken) { result in
            switch result {
            case .success(let data):
                guard let data = data as? JSONDictionary, let requestToken = data["request_token"] as? String else { return }
                self.logIn(requestToken, username: username, password: password, onCompletion: onCompletion)
            case .failure(let error):
                print(error)
            }
        }
    }
}

// MARK: Authentication
private extension SessionManager {
    func logIn(_ requestToken: String, username: String, password: String, onCompletion: @escaping APIResponse) {
        APIClient.shared.request(request: AuthRoute.login(requestToken: requestToken,
                                                          username: username, password: password)) { result in
            switch result {
            case .success: self.createSession(requestToken, onCompletion: onCompletion)
            case .failure(let error): onCompletion(.failure(error))
            }
        }
    }
    
    func createSession(_ requestToken: String, onCompletion: @escaping APIResponse) {
        APIClient.shared.request(request: AuthRoute.createSession(requestToken)) { result in
            switch result {
            case .success(let data):
                guard let data = data as? JSONDictionary, let sessionId = data["session_id"] as? String else {
                    onCompletion(.failure(APIError.createSession))
                    return
                }
                self.sessionId = sessionId
                self.getUser(onCompletion: onCompletion)
            case .failure(let error): onCompletion(.failure(error))
            }
        }
    }
    
    func getUser(onCompletion: @escaping APIResponse) {
        APIClient.shared.requestItem(request: UserRoute.getAccount) { (result: Result<User, Error>) in
            switch result {
            case .success(let user):
                self.currentUser = user
                onCompletion(.success(0))
            case .failure(let error): onCompletion(.failure(error))
            }
        }
    }
}

// MARK: Favorite
extension SessionManager {
    func setFavorite(movieId: Int, value: Bool, onCompletion: @escaping APIResponse) {
        guard let accountId = currentUser?.id else {
            onCompletion(.failure(APIError.getUser))
            return
        }
        APIClient.shared.request(request: UserRoute.setFavorite(accountId: accountId, mediaId: movieId, value: value), onCompletion: onCompletion)
    }
}
