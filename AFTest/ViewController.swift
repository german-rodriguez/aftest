//
//  ViewController.swift
//  AFTest
//
//  Created by German Rodriguez on 7/20/21.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        SessionManager.shared.logIn(username: "TestGerman1", password: "1234") { result in
            switch result {
            case .success:
                self.setFavorite()
            case .failure(let error):
                print("ERROR: \(error)")
            }
        }
    }
    
    private func setFavorite() {
        SessionManager.shared.setFavorite(movieId: 550, value: true) { result in
            switch result {
            case .success(let data):
                print("SET FAVORITE: \(data)")
            case .failure(let error):
                print(error)
            }
        }

    }
}
